<?php

class config{
	private $host;
	private $username;
	private $password;
	private $dbname;
	protected $connect;


	function __construct(){
		$this->host= "localhost";
		$this->username= "root";
		$this->password= "password";
		$this->dbname= "a-zclothes";
		$this->getConnection();
	}

	function getConnection(){
		$this->connect= new mysqli($this->host, $this->username, $this->password, $this->dbname);
		if($this->connect->connect_error){
			die("error:" .$this->connect->error);

		}else{
			return $this->connect;
		}
	}
}


// $hostname= "localhost";
// $username= "root";
// $password= "password";
// $databaseName= "a-zclothes";

// $connection= mysqli_connect($hostname, $username, $password, $databaseName);

// if($connection->connect_errno){
// 	print "failed to connect to the database";
// }



?>

 