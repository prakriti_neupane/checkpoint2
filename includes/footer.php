<footer>
<div class="container">
<div class="row">
  <div class="foot-logo">
    <div class="col-md-2 col-sm-2">
      <figure>
        <a href="../index.php"><img src="../assets/images/logo.jpeg" alt="images"></a>
      </figure>
    </div>
  </div>
  <div class="footer-widget">
    <div class="col-md-3 col-sm-3">
     <h4>Recent Activities</h4>
      <ul class="footer-menu">
        <li><a href="../index.php">home</a></li>
        <li><a href="#">Men</a></li>
        <li><a href="#">Women</a></li>
        <li><a href="#">Kids</a></li>
        <li><a href="../views/about.php">About</a></li>
        <li><a href="../views/contact.php">Contact</a></li>
      </ul>
    </div>
  </div>
  <div class="footer-widget">
    <div class="col-md-4 col-sm-4">
      <!-- <div class="footer-news">
        <h4>Recent Activities</h4>
        <ul>
          <li>
            <a href="news.html">Press Conference By QGL CEO Nicolas Ralph</a>
            <span class="post-date">March 18, 2017</span>
          </li>
          <li>
            <a href="news.html">14964</a>
            <span class="post-date">February 24, 2017</span>
          </li>
          <li>
            <a href="news.html">Important Message From Group CEO</a>
            <span class="post-date">February 24, 2017</span>
          </li>
          <li>
            <a href="news.html">CEO</a>
            <span class="post-date">February 24, 2017</span>
          </li>
        </ul>
      </div> -->
    </div>
  </div>
  <div class="footer-widget">
    <div class="col-md-3 col-sm-3">
      <div class="contact">
        <a href="contact-us.html"><i class="fa fa-envelope-o"></i><span>Contact Us Now ❯</span></a>
      </div>
      <div class="social-sec">
        <ul>
          <li> <a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-youtube"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>

        </ul>
      </div>
    </div>
  </div>
</div>
<div class="col-md-8 col-md-offset-2">
  <div class="textwidget">© Copyright A-Z Clothing Store.All Rights Reserved
</div>
</div>
</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/owl-carousel.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>

<script>
  function init() {
  gapi.load('auth2', function() { // Ready. });
}
</script>


</body>
</html>