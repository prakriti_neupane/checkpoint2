<?php
// include('db_connection.php');
class adminFunctions extends config
{
	private $table_name;
    private $id;
	
	//variables for admin login
	private $username;
	private $password;


   //variables for product 
    private $product_id;
	private $product_name;
    private $price;
    private $category_id;
    private $product_image;
    private $product_description;


    //setters and getters for user login


    public function gettableName()
    {
		return $this->tableName;
	}

     public function getId()
    {
        return $this->id;
    }


	public function getUsername()
	{
		return $this->username;
	}

	public function getPassword()
	{
		return $this->password;
	}
    
    public function settableName($table_name)
    {
      $this->tableName= $table_name;
    }

    public function setId($id)
    {
      $this->id= $id;
    }

    public function setUsername($username)
    {
    	$this->username= $username;
    } 

    public function setPassword($password)
    {
    	$this->password= $password;
    }


   //setters and getters for clothes

     public function getclothesId()
    {
        return $this->product_id;
    }


    public function getproductName()
    {
    	return $this->product_name;
    }

    public function getPrice()
    {
    	return $this->price;
    }

    public function getcatId()
    {
        return $this->category_id;
    }


    public function getproductImage()
    {
    	return $this->product_image;
    }

    public function getproductDes()
    {
        return $this->product_description;
    }


  
  public function setclothesId($product_id)
  {
    $this->product_id= $product_id;
  }

    public function setproductName($product_name)
    {
    	$this->product_name= $product_name;
    }

    public function setPrice($price)
    {
    	$this->price= $price;
    }


    public function setcatId($category_id){
     $this->category_id= $category_id;
    }

    public function setproductImage($product_image)
    {
        $this->product_image= $product_image;
    }

     public function setproductDes($product_description)
    {
        $this->product_description= $product_description;
    }



    //function to select data from table
	public function fetchData()
	{
    	return mysqli_query($this->connect, "SELECT * FROM ".$this->tableName);         
	}

//function for admin login
public function login()
{

	$sql = "SELECT * FROM $this->tableName WHERE username = '$this->username' && password = '$this->password'"; 
	return mysqli_query($this->connect, $sql);
}



 public function insertProducts()
 {
 	$insert= "INSERT INTO $this->tableName(`product_name`, `price`,  `category_id`,  `product_image`, `product_description`) VALUES('$this->product_name', '$this->price', '$this->category_id', '$this->product_image', '$this->product_description')";
    //var_dump($insert); die();
 	return mysqli_query($this->connect, $insert);
 }


 public function insertbookableProducts()
 {
    $insertBooking= "INSERT INTO $this->tableName(image) VALUES('$this->product_image')";
    //var_dump($insertBooking); die();
    return mysqli_query($this->connect, $insertBooking);
 }


// selecting clothes categorized by id
 public function selectbyId()
 {
    $select = "SELECT * FROM $this->tableName WHERE category_id= '$this->category_id'";
    //var_dump($select); die();
    return mysqli_query($this->connect, $select);
 }

//selecting to update data
 public function getbyId()
 {
    $select = "SELECT * FROM $this->tableName WHERE product_id= '$this->product_id'";
    return mysqli_query($this->connect, $select);
 }

//updating product
 public function updateData()
 {
    // if(isset($this->product_image)){
        $update = "UPDATE $this->tableName SET product_name= '$this->product_name', price= '$this->price',  category_id= '$this->category_id',  product_image= '$this->product_image', product_description= '$this->product_description'  WHERE product_id= '$this->product_id'";

     //var_dump($update); die();
    // }else{
        // $update = "UPDATE $this->tableName SET product_name= '$this->product_name', price= '$this->price',  category_id= '$this->category_id',  product_description= '$this->product_description'  WHERE product_id= '$this->product_id'";
    // var_dump($update);
    // die();
    return mysqli_query($this->connect, $update);
 }



//query to delete the data
 public function deleteproduct()
 {
    $removeproduct= "DELETE FROM $this->tableName WHERE product_id = '$this->product_id'";
    return mysqli_query($this->connect, $removeproduct);
 }


//select recently added items
 public function recentItem()
 {
    $recentAdded= "SELECT * FROM $this->tableName ORDER BY product_id DESC LIMIT 4";
    return mysqli_query($this->connect, $recentAdded);
 }


}

?>