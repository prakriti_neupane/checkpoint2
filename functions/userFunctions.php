<?php
class userFunctions extends config
{
	private $tableName;
	private $columnName;
	private $username;
	private $user_id;
	private $password;
	private $fullname;
	private $gender;
	private $phone;
	private $email;
	private $address;
	private $date;
	private $product_image;


	public function gettableName()
	{
		return $this->tableName;
	}

	public function getcolumnName()
	{
		return $this->columnName;
	}
    
    public function getUsername()
	{
		return $this->username;
	}

	public function getUserId()
	{
		return $this->user_id;
	}


	public function getPassword()
	{
		return $this->password;
	}


	public function getfullName()
	{
		return $this->fullname;
	}


	public function getGender()
	{
		return $this->gender;
	}


	public function getNumber()
	{
		return $this->phone;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function getAddress()
	{
		return $this->address;
	}


	public function getdate()
	{
		return $this->date;
	}

	public function getproductImage()
	{
		return $this->tableName;
	}

	public function settableName($tableName)
	{
		$this->tableName= $tableName;
	}

	public function setcolumnName($columnName)
	{
		$this->columnName= $columnName;
	}


	public function setUsername($username)
	{
		$this->username= $username;
	} 

	public function setUserId($user_id)
	{
		$this->user_id= $user_id;
	} 

	public function setPassword($password)
	{
		$this->password= $password;
	}

	public function setfullName($fullname)
	{
		$this->fullname= $fullname;
	}

	public function setGender($gender)
	{
		$this->gender= $gender;
	}

	public function setNumber($phone)
	{
		$this->phone= $phone;
	} 

	public function setEmail($email)
	{
		$this->email= $email;
	}

	public function setAddress($address)
	{
		$this->address= $address;
	} 


	public function setDate($date)
	{
		$this->date= $date;
	}


	public function setproductImage($product_image)
	{
		$this->product_image= $product_image;
	}


public function selectAllItems()
	{
		$selectProducts= "SELECT * FROM $this->tableName";
		return mysqli_query($this->connect, $selectProducts);

	}


	public function checkUser($username)
	{
		$query= "SELECT `username` FROM $this->tableName WHERE username= '$username'";
		//var_dump($query); die();
		return mysqli_query($this->connect, $query);
	}


	public function userReg()
	{
		$reg= "INSERT INTO $this->tableName(`username`,`password`, `fullname`, `gender`, `phone`, `email`, `address`) VALUES ('$this->username', '$this->password', '$this->fullname', '$this->gender', '$this->phone', '$this->email', '$this->address')";
		//var_dump($reg); die();
		return mysqli_query($this->connect, $reg);

	}

	public function userSignin()
	{
		$login= "SELECT * FROM $this->tableName WHERE `username`= '$this->username' && `password`= '$this->password'";
		return mysqli_query($this->connect, $login);
	}


public function bookProducts()
	{
		$reg= "INSERT INTO $this->tableName(`date`, `booked_product`, `user_id`) VALUES ('$this->date', '$this->product_image', '$this->user_id')";
		//var_dump($reg); die();
		return mysqli_query($this->connect, $reg);
	}
}
?>
