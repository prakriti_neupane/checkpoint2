  
<?php 

include('includes/header.php'); 
include('../includes/config.php');
?>
<body class="">
    <div class="wrapper ">

        <?php include('includes/sidebar.php'); ?>
        
        <?php
        include('../functions/adminFunctions.php');
        if(isset($_POST['additem'])){
            $product_img = addslashes(file_get_contents($_FILES["imgfile"]["tmp_name"]));
            $addItems = new adminFunctions();
             $addItems-> settableName('product_booking');
             $addItems->setproductImage($product_img);


            $addImg = $addItems->insertbookableProducts();
               //var_dump($addImg); die();
            
            if($addImg==true){
                echo "<script>alert('data inserted')</script>";
                header("location: index.php");
                
            }else{
                echo "error to insert";
            }
        }



            ?>

  <div class="main-panel">
         <div class="container">
            <div class="row main">
                <div class="main-login main-center">
                    <h5>Add Bookable Products</h5>
                    <form class="book-form" method="post"  enctype="multipart/form-data">

                        <div class="form-group">
                            <label for="file" class="cols-sm-2 control-label">Upload product Image</label>
                            <div class="cols-sm-10">
                                <span class="btn btn-default btn-file">
                                    Browse <input type="file" id="image" name="imgfile" required="" accept=".jpg, .jpeg, .png">
                                </span>
                            </div>
                        </div>

                        <div class="form-group ">
                            <button type="submit" id="insert" name= "additem" class="btn btn-primary btn-lg btn-block login-button">Add Item</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php 
include('includes/footer.php'); ?>

    <script>
    $(document).ready(function(){
        $('#insert').click(function(){
         var image_name= $('#profile_pic').val();
         if(image_name== ''){
            alert("select image");
        }
        else{
            var extension= $('#image').val().split('.').pop().toLowerCase();
            if(jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1){
                alert('invalid image file');
                $('#image').val('');
                return false;
            }
        }

    });
    });

</script>
