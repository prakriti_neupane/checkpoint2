<?php
 include('includes/header.php'); 
 include('../includes/config.php');
include('../functions/adminFunctions.php');

?>

<body class="">
    <div class="wrapper ">
       <?php include('includes/sidebar.php'); ?>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <a class="navbar-brand" href="#pablo">Table List</a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <form>
                            <div class="input-group no-border">
                                <input type="text" value="" class="form-control" placeholder="Search...">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_zoom-bold"></i>
                                </span>
                            </div>
                        </form>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#pablo">
                                    <i class="now-ui-icons media-2_sound-wave"></i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Stats</span>
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="now-ui-icons location_world"></i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Some Actions</span>
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#pablo">
                                    <i class="now-ui-icons users_single-02"></i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Account</span>
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="panel-header panel-header-sm">
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">LADIES ITEMS</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">

                                            <th>
                                               Product ID
                                            </th>


                                            <th>
                                               Product Name
                                            </th>

                                            <th>
                                                Price
                                            </th>
                            
                                            <th>
                                                Product Image
                                            </th>

                                            <th>
                                                Product Description
                                            </th>
                                            
                                        </thead>
                                        <tbody id= "bookData">
                                           <?php 
                                           $viewall= new adminFunctions();
                                           $viewall->  settableName('product');
                                           $viewall-> setcatId(1);
                                           $select= $viewall->selectbyId();
                                           if($select->num_rows > 0){
                                               while($row= $select->fetch_assoc()){
                                                  ?>
                                                  <tr>
                                                    <td>
                                                       <?php echo $row["product_id"] ?>
                                                   </td>
                                                   <td>
                                                    <?php echo $row["product_name"] ?>
                                                </td>
                                                <td>
                                                   Rs.<?php echo $row["price"] ?>
                                               </td>
                                              <td class="text-right">
                                                  <?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['product_image'] ).'" height="100" width="200" class="img-thumnail" />';  ?>
                                              </td>

                                              <td>
                                                   <?php echo $row["product_description"]; ?>
                                               </td>

                                              <td><a href= "update.php?product_id=<?php echo $row['product_id']; ?>"><button>Edit</button></a></td>
                                              <td><a href= "deleteData.php?book_id=<?php echo $row['book_id']; ?>"><button>Delete</button></a></td>

                                          </tr>
                                           
                                            <?php  }
                                        }else{
                                            echo "No record found here";
                                        }

                                        ?>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="card card-plain">
                            <div class="card-header">
                                <h4 class="card-title">ZENS ITEMS</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">

                                            <th>
                                               Product ID
                                            </th>


                                            <th>
                                               Product Name
                                            </th>

                                            <th>
                                                Price
                                            </th>
                            
                                            <th>
                                                Product Image
                                            </th>

                                            <th>
                                                Product Description
                                            </th>
                                            
                                        </thead>
                                        <tbody id= "bookData">
                                           <?php 
                                           $mensProduct= new adminFunctions();
                                           $mensProduct->  settableName('product');
                                           $mensProduct-> setcatId(2);
                                           $res= $mensProduct->selectbyId();
                                           if($res->num_rows > 0){
                                               while($row= $res->fetch_assoc()){
                                                  ?>
                                                  <tr>
                                                    <td>
                                                       <?php echo $row["product_id"] ?>
                                                   </td>
                                                   <td>
                                                    <?php echo $row["product_name"] ?>
                                                </td>
                                                <td>
                                                   Rs.<?php echo $row["price"] ?>
                                               </td>
                                              <td class="text-right">
                                                  <?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['product_image'] ).'" height="100" width="200" class="img-thumnail" />';  ?>
                                              </td>

                                              <td>
                                                   <?php echo $row["product_description"]; ?>
                                               </td>

                                              <td><a href= "update.php?product_id=<?php echo $row['product_id']; ?>"><button>Edit</button></a></td>
                                              <td><a href= "deleteData.php?book_id=<?php echo $row['book_id']; ?>"><button>Delete</button></a></td>

                                          </tr>
                                           
                                            <?php  }
                                        }else{
                                            echo "No record found here";
                                        }

                                        ?>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                      <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">KIDS ITEMS</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">

                                            <th>
                                               Product ID
                                            </th>


                                            <th>
                                               Product Name
                                            </th>

                                            <th>
                                                Price
                                            </th>
                            
                                            <th>
                                                Product Image
                                            </th>

                                            <th>
                                                Product Description
                                            </th>
                                            
                                        </thead>
                                        <tbody id= "bookData">
                                           <?php 
                                           $selectkids= new adminFunctions();
                                           $selectkids->  settableName('product');
                                           $selectkids-> setcatId(3);
                                           $result= $selectkids->selectbyId();
                                           if($select->num_rows > 0){
                                               while($row= $result->fetch_assoc()){
                                                  ?>
                                                  <tr>
                                                    <td>
                                                       <?php echo $row["product_id"] ?>
                                                   </td>
                                                   <td>
                                                    <?php echo $row["product_name"] ?>
                                                </td>
                                                <td>
                                                   Rs.<?php echo $row["price"] ?>
                                               </td>
                                              <td class="text-right">
                                                  <?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['product_image'] ).'" height="100" width="200" class="img-thumnail" />';  ?>
                                              </td>

                                              <td>
                                                   <?php echo $row["product_description"]; ?>
                                               </td>

                                              <td><a href= "update.php?product_id=<?php echo $row['product_id']; ?>"><button>Edit</button></a></td>
                                              <td><a href= "deleteData.php?book_id=<?php echo $row['book_id']; ?>"><button>Delete</button></a></td>

                                          </tr>
                                           
                                            <?php  }
                                        }else{
                                            echo "No record found here";
                                        }

                                        ?>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
            <?php include('includes/footer.php'); ?>