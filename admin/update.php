<?php 
include('includes/header.php'); 
include('../includes/config.php');
?>
<body class="">
    <div class="wrapper ">
        <?php include('includes/sidebar.php'); ?>
        <div class="container">
            <?php
            include('../functions/adminFunctions.php');
            $db = new config();
            $conn = $db->getConnection();
            if(isset($_GET['product_id']))
            {
                $selectId = new adminFunctions();
                $selectId->settableName('product');
                $selectId->setclothesId(trim($_GET['product_id']));
                $result = $selectId->getbyId();
                if($result->num_rows==1){
                    foreach($result as $value)
                    {
                        $id= $value['product_id'];
                        $name= $value['product_name'];
                        $price= $value['price'];
                        $category_id= $value['category_id'];
                        $desc= $value['product_description'];
                    }
                }
            }

            if(isset($_POST['update'])){
            // $id= mysqli_escape_string()($conn, $_POST['book_id']);
                $name= mysqli_real_escape_string($conn,$_POST['product_name']);
                $price= mysqli_real_escape_string($conn, $_POST['price']);
                $category= mysqli_real_escape_string($conn, $_POST['category']);
                $description= mysqli_real_escape_string($conn, $_POST['desc']);





                $product_id= $_POST['product_id'];
                $product_name = $_POST['product_name'];
                $price = $_POST['price'];
                $category_id =(int) ($_POST['category']);
                $description= $_POST['desc'];

                $updateItem = new adminFunctions();
                $updateItem->settableName('product');
                $updateItem->setclothesId($_POST['product_id']);
                $updateItem->setproductName($product_name);
                $updateItem->setPrice($price);
                $updateItem->setcatId($category_id);
                $updateItem->setproductDes($description);
                // if(isset($book_image)){
                    $product_image = addslashes(file_get_contents($_FILES["imgfile"]["tmp_name"]));
                    $updateItem->setproductImage($product_image);
                // }


                $UpdateQuery = $updateItem->updateData($_POST['product_id']);
                //var_dump($UpdateQuery); die();
                if($UpdateQuery){
                   echo "<script> window.alert('Data Updated successfully');
                   window.location.href = 'tables.php';
                   </script>";

               }else{
                echo "<script>alert('Error to update Data')</script>";
            }
        }

        ?>
        <div class="main-panel">
         <div class="container">
            <div class="row main">
                <div class="main-login main-center">
                    <h5>Add New Book lists here</h5>
                    <form class="book-form" method="post"  enctype="multipart/form-data">

                      <div class="form-group">
                        <label for="bookname" class="cols-sm-2 control-label">Product</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-book fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="product_id" id="product_id" readonly="readonly" required="" value= "<?php echo $id;?>" placeholder="Enter Book  ID"/>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="bookname" class="cols-sm-2 control-label">Product Name</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-book fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="product_name" id="name" required="" value= "<?php echo $name; ?>"  placeholder="Enter Product Name"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="price" class="cols-sm-2 control-label">Price</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fas fa-dollar-sign" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="price" id="cost" value= "<?php echo $price ?>"required="" placeholder="Enter Price"/>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="genere" class="cols-sm-2 control-label">Product Category</label>
                        <select class="form-control" id="cat" name="category" required="">
                            <option disabled="" val="" selected="hidden">Choose Category</option>
                            <?php 
                            $selectCat= new adminFunctions();
                            $selectCat-> settableName('category');
                            $result= $selectCat->fetchData();
                            if($result->num_rows>0){
                                while($row=$result->fetch_array())
                                {  
                                    if($row['category_id'] == $category_id){
                                        echo "<option value='$row[category_id]' selected>" .$row['category_name']. "</option>";
                                    }else{
                                        echo "<option value='$row[category_id]'>" .$row['category_name']. "</option>";
                                    }
                                }
                            }else{
                                echo "<option value= '' disabled='' selected='hidden'>No any category</option>";
                            }
                            ?>

                        </select>
                    </div>


                    <div class="form-group">
                        <label for="file" class="cols-sm-2 control-label">Upload Product Image</label>
                        <div class="cols-sm-10">
                            <span class="btn btn-default btn-file">
                                Browse <input type="file" id="image" name="imgfile" accept=".jpg, .jpeg, .png">
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                            <label for="description" class="cols-sm-2 control-label">Description</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-dollar-sign" aria-hidden="true"></i></span>
                                    <textarea class="form-control" value= "<?php echo $desc; ?>" name="desc" id="desc"  required=""  placeholder="Enter Short Description" rows="3"></textarea>
                                </div>
                            </div>
                        </div>



                    <div class="container">

                        <div class="form-group ">
                            <button type="submit" id="update" name= "update" class="btn btn-primary btn-lg btn-block login-button">Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>



</div>
</div>
<?php 
include('includes/footer.php'); ?>

<script>
    $(document).ready(function(){
        $('#insert').click(function(){
         var image_name= $('#profile_pic').val();
         if(image_name== ''){
            alert("select image");
        }
        else{
            var extension= $('#image').val().split('.').pop().toLowerCase();
            if(jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1){
                alert('invalid image file');
                $('#image').val('');
                return false;
            }
        }

    });
    });

</script>