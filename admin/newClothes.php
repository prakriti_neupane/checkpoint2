  
<?php 

include('includes/header.php'); 
include('../includes/config.php');
?>
<body class="">
    <div class="wrapper ">

        <?php include('includes/sidebar.php'); ?>
        
        <?php
        include('../functions/adminFunctions.php');
        if(isset($_POST['additem'])){
            $product_name = $_POST['product_name'];
            $price = $_POST['price'];
            $category_id =(int) ($_POST['category']);
            $product_image = addslashes(file_get_contents($_FILES["imgfile"]["tmp_name"]));
            $product_desc= $_POST['desc'];

            $addItem = new adminFunctions();
            $addItem->settableName('product');
            $addItem->setproductName($product_name);
            $addItem->setPrice($price);
            $addItem->setcatId($category_id);
            $addItem->setproductImage($product_image);
            $addItem->setproductDes($product_desc);

            $addQuery = $addItem->insertProducts();
              // var_dump($addQuery);
            
            if($addQuery==true){
                echo "<script>alert('data inserted')</script>";
                header("location: index.php");
                
            }else{
                echo "error to insert";
            }
        }
            ?>

  <div class="main-panel">
         <div class="container">
            <div class="row main">
                <div class="main-login main-center">
                    <h5>Add New Clothes here</h5>
                    <form class="book-form" method="post"  enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="bookname" class="cols-sm-2 control-label">Product Name</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-book fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="product_name" id="name" required=""  placeholder="Enter Product Name"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="price" class="cols-sm-2 control-label">Price</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-dollar-sign" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="price" id="cost" required="" placeholder="Enter Price"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="genere" class="cols-sm-2 control-label">Product Category</label>
                            <select class="form-control" id="cat" name="category" required="">
                                <option disabled="" val="" selected="hidden">Choose Category</option>
                                <?php 
                                $selectCat= new adminFunctions();
                                $selectCat-> settableName('category');
                                $result= $selectCat->fetchData();
                                if($result->num_rows>0){
                                    while($row=$result->fetch_array())
                                    {  
                                        echo "<option value='$row[category_id]'>" .$row['category_name']. "</option>";
                                    }
                                }else{
                                    echo "<option value= '' disabled='' selected='hidden'>No any category</option>";
                                }
                                ?>

                            </select>
                        </div>


                        <div class="form-group">
                            <label for="file" class="cols-sm-2 control-label">Upload product Image</label>
                            <div class="cols-sm-10">
                                <span class="btn btn-default btn-file">
                                    Browse <input type="file" id="image" name="imgfile" required="" accept=".jpg, .jpeg, .png">
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="price" class="cols-sm-2 control-label">Description</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-dollar-sign" aria-hidden="true"></i></span>
                                    <textarea class="form-control" name="desc" id="desc" required="" placeholder="Enter Short Description"></textarea>
                                </div>
                            </div>
                        </div>



                        <div class="form-group ">
                            <button type="submit" id="insert" name= "additem" class="btn btn-primary btn-lg btn-block login-button">Add Item</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php 
include('includes/footer.php'); ?>

    <script>
    $(document).ready(function(){
        $('#insert').click(function(){
         var image_name= $('#profile_pic').val();
         if(image_name== ''){
            alert("select image");
        }
        else{
            var extension= $('#image').val().split('.').pop().toLowerCase();
            if(jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1){
                alert('invalid image file');
                $('#image').val('');
                return false;
            }
        }

    });
    });

</script>
