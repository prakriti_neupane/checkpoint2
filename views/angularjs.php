
<?php include('../includes/header.php') ?>

    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <style>
table, th , td  {
  border: 1px solid grey;
  border-collapse: collapse;
  padding: 5px;
}
table tr:nth-child(odd) {
  background-color: #f1f1f1;
}
table tr:nth-child(even) {
  background-color: #ffffff;
}
</style>

<body>
<h2>AngularJS HTTP/MySQL/JSON and Search Filters</h2>
<p>
This example uses AngularJS to connect to MySQL data at https://a-z-clothing-store.000webhostapp.com/json.php. This is output as JSON and then displayed using an AngularJS controller. The results can be filtered by the entries in the search box. 
</p>  
<div  ng-controller="userCtrl"> 
<br />
<br />
<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>Username</th>
      <th>Fullname</th>
      <th>Phone</th>
      <th>Email</th>
      <th>Address</th>
    </tr>
    <tr>
      <th><input type="text" ng-model="search.user_id"/></th>
      <th><input type="text" ng-model="search.username"/></th>
      <th><input type="text" ng-model="search.fullname"/></th>
      <th><input type="text" ng-model="search.phone"/></th>
      <th><input type="text" ng-model="search.email"/></th>
      <th><input type="text" ng-model="search.address"/></th>
    </tr>
  </thead>
  <tbody>
    <tr ng-repeat="user in sc_user | filter:search">
      <td>{{user.user_id}}</td>
      <td>{{user.username}}</td>
      <td>{{user.fullname}}</td>
      <td>{{user.phone}}</td>
      <td>{{user.email}}</td>
      <td>{{user.address}}</td>
    </tr>
  </tbody>
</table>

</div>
<?php include('../includes/footer.php'); ?>

<script type="text/javascript">
  var userModule=angular.module('userModule',[]);
  userModule.controller('userCtrl',function($scope,$http){
    // used to filter records by attributes
    $scope.search={};

    // get data from the server in JSON format
    $http.get('json.php').success(function(data,status,header,config){
      $scope.sc_user=data;
    });
  });
</script>