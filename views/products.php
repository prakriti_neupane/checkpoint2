<?php
include ("../includes/config.php");
include("../functions/userFunctions.php");
$connection= new config();
$setConn= $connection->getConnection();
$allProducts= new userFunctions();
$allProducts-> settableName('product');
$products= $allProducts-> selectAllItems();
if ($products->num_rows > 0) {

    /* fetch associative array */
    while($row= $products->fetch_assoc()){

       array_push($booksArray, $row);
    }
  
    if(count($booksArray)){

         createXMLfile($booksArray);

     }

    /* free result set */
    $result->free();
}

/* close connection */
$mysqli->close();

function createXMLfile($booksArray){
  
   $filePath = 'productlist.xml';

   $dom     = new DOMDocument('1.0', 'utf-8'); 

   $root      = $dom->createElement('product'); 

   for($i=0; $i<count($booksArray); $i++){
     
     $bookId        =  $booksArray[$i]['product_id'];  

     $bookName      =  $booksArray[$i]['product_name']; 

     // $bookAuthor    =  $booksArray[$i]['author_name']; 

     $bookPrice     =  $booksArray[$i]['price']; 

     // $bookISBN      =  $booksArray[$i]['ISBN']; 

     $bookCategory  =  $booksArray[$i]['category_id'];	

     $book = $dom->createElement('product');

     $book->setAttribute('product_id', $bookId);

     $name     = $dom->createElement('product_name', $bookName); 

     $book->appendChild($name); 

     // $author   = $dom->createElement('author', $bookAuthor); 

     // $book->appendChild($author); 

     $price    = $dom->createElement('price', $bookPrice); 

     $book->appendChild($price); 

     // $isbn     = $dom->createElement('ISBN', $bookISBN); 

     // $book->appendChild($isbn); 
     
     $category = $dom->createElement('category_id', $bookCategory); 

     $book->appendChild($category);
 
     $root->appendChild($book);

   }

   $dom->appendChild($root); 

   $dom->save($filePath); 

 } 
