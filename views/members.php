
<?php
session_start();
include('../includes/header.php');
include('../includes/config.php');
include('../functions/userFunctions.php');
$connection= new config();
$setConn= $connection->getConnection();
?>
<!-- Breadcrumb Section -->
<!-- <section class="breadcrumb_wrap" style="background: url(images/about-bg.jpg) no-repeat fixed center center / cover;">
		
	</style>>
	<ul class="breadcrumb">
		<li>IELTS</li>
	</ul>
</section> -->
<script>
	$( function() {
		$( "#dialog" ).dialog();
	} );
</script>

</div>
</div>
</section>

<section id="detail-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-9">
				<div id="mt-primary">
					<div class="mt-inner-contents">
						<figure>
							<img class="mt-banner" src="../images/course5.jpg" alt="">
						</figure>
					</div>

					<?php
					if(isset($_POST['signup']))
					{
						$username= mysqli_real_escape_string($setConn, trim($_POST['username']));
						$password= mysqli_escape_string($setConn, trim($_POST['password']));
						$fullname= mysqli_escape_string($setConn, trim($_POST['fullname']));
						$gender= mysqli_escape_string($setConn, trim($_POST['gender']));
						$phone= mysqli_escape_string($setConn, trim($_POST['number']));
						$email= mysqli_escape_string($setConn, trim($_POST['email']));
						$address= mysqli_escape_string($setConn, trim($_POST['address']));



						$memberReg= new userFunctions();
						$memberReg->settableName('sc_user');
						$memberReg->setUsername($username);
						$memberReg->setPassword(md5($password));
						$memberReg->setfullName($fullname);
						$memberReg->setGender($gender);
						$memberReg->setNumber($phone);
						$memberReg->setEmail($email);
						$memberReg->setAddress($address);

						$regQuery= $memberReg->userReg();
						if($regQuery==true)
						{
							echo "<script> alert ('Invalid username or password');
      location.replace('memberPanel.php');

      </script>";
						}else{
							echo "<script> alert ('Ops! Something went wrong') </script>;";
						}
					}

					?>

					<?php
					if(isset($_POST['signin']))
					{
						$username= $_POST['lguser'];
						$password= $_POST['lgpass'];

						$userLogin= new userFunctions();
						$userLogin-> settableName('sc_user');
						$userLogin-> setUsername($username);
						$userLogin->setPassword(md5($password));

						$loginQuery= $userLogin-> userSignin();
					 	//var_dump($loginQuery); die();
						if($loginQuery->num_rows==1)
						{
							while($row= $loginQuery->fetch_array())
							{
								$_SESSION['id']= $row['user_id'];
								echo "<script> location.replace('memberPanel.php'); </script>";
							}
						}
						else{
							 echo "<script> alert ('Invalid username or password');
      location.replace('members.php');

      </script>";
						}                                                                        

					}

					?>

					<div id='dialog' title='Info Box'>
								<p>Login credentials for entering into user panel. <strong>Username:</strong>admin <strong>Password:</strong> admin
								 </p>
							</div>

					<div class="mt-inner-contents">
					</div>
					<div class="mt-inner-contents">
						<form class="registration-form" method= "post">
							<div class="form-group">
								<label class="sr-only" >Username</label>
								<input type="text" class="form-control" id= "usrname" name= "username" placeholder="Username">
							</div>

							<div id= "displayError" style="color: red;">
								<!-- <p>Username is taken</p> -->


							</div>

							<div id= "displaySuccess" style="color: green">
								
							</div>

							<div class="form-group">
								<label class="sr-only" >Password</label>
								<input type="password" class="form-control"  placeholder="Password" name= "password">
							</div>

							<div class="form-group">
								<label class="sr-only" >Full Name</label>
								<input type="text" class="form-control" name= "fullname"  placeholder="Full Name">
							</div>

							<div class="form-group">

								<select class="form-control" id="txtpdest" name="gender" required="">
									<option val="" selected="">Gender</option>
									<option val="ielts">Male</option>
									<option val="sat">Female</option>
									<option val="gre">Others</option>
								</select>

							</div>
							<div class="form-group">
								<label class="sr-only">Phone</label>
								<input type="text" name= "number" class="form-control"  placeholder="Phone">
							</div>
							<div class="form-group">
								<label class="sr-only">Email</label>
								<input type="Email" class="form-control" name= "email" placeholder="Email">
							</div>
							<div class="form-group">
								<label class="sr-only">Address</label>
								<input type="text" class="form-control" name= "address" placeholder="Address">
							</div>


							<div class="fnt10">
								<input name="txtagree" id="txtagree" value="yes" required="" type="checkbox"> I agree to the Terms of Service and Privacy Policy.
								<button id="submit" class="mt-btn" name= "signup" type= "submit">Register</button>
							</div>
						</form>

						


					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-3">
				<div id="mt-secondary">
					<div class="mt-inner-contents other-links">
						<h4>Services for members</h4>
						<li><a href="#">Can book clothes in rent.<i class="fa fa-angle-right pull-right"></i></a></li>
						<li><a href="#">Can get home delivery service<i class="fa fa-angle-right pull-right"></i></a></li>
						<li><a href="#">Can order your design<i class="fa fa-angle-right pull-right"></i></a></li>
						<li><a href="#">Can get discount offers<i class="fa fa-angle-right pull-right"></i></a></li>
					</div>

					<div class="mt-inner-contents">
						<h4>Login if you are already registered</h4>
						<form method= "post">

							<div class="form-group">

								<input id="username" class="form-control" placeholder="Username" required="" name= "lguser" type="text">


							</div>
							<div class="form-group">
								<input id="password" class="form-control" placeholder="Password" required="" name= "lgpass" type="password">

							</div>
							<br>
							<div class="form-group">
								<button type="submit" id="btnSubevnt" name= "signin" class="btn btn-lg btn-block btn-success btn-blue3">Login</button>
							</div>

							<div class="g-signin2" data-onsuccess="onSignIn"></div>
						</form>
					</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<?php
	include('../includes/footer.php');

	?>
	<script>
		$(document).ready(function(){
		// alert("I am loaded");


		// $('#reg').on('click', function(){
		// 	alert("Clicked");
		// });


		$('#usrname').on('blur', function(){
			//alert ("user");
			var username= $(this).val();
			
			$.ajax({
				type: "POST",
				url: "ajaxView.php",
				data: { user: username},
				success: function(data){
					var response = parseInt(data);
					//alert(response);
					if(response == 1){
						//alert(response);
						//alert('go to error');
						$('#displayError').html("Username Exists");
						$('#submit').hide();

					}
					else {
						//alert('go to success');
						//alert(response);
						$('#displaySuccess').html("Username is Available");
						$('#submit').show();

					}

				},

				error: function(data){
					alert(response);
				 	$('#displayError').html(data);
				}
			})
		});
	});

</script>
<!-- <script>
$(document).ready(function(){
$('#displaySuccess').on('mousemove', function(){
	//alert('hide');
	$('#submit').hide();

})
});

$(document).ready(function(){
$('#displayError').on('mousemove', function(){
	//alert('js');
	$('#submit').show();

})
});

</script> -->
