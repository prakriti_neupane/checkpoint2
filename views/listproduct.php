<?php 
header ("Content-Type:text/xml");
include ("../includes/config.php");
include("../functions/userFunctions.php");
$connection= new config();
$setConn= $connection->getConnection();
$allProducts= new userFunctions();
$allProducts-> settableName('product');
$products= $allProducts-> selectAllItems();
$_xml = '<?xml version="1.0"?>'; 
$_xml= '<?xml-stylesheet type="text/xsl" href="../assets/css/test.xsl"?>';
$_xml .="<product_list>"; 
while($row = mysqli_fetch_array($products)) { 
$_xml .="<product>"; 
$_xml .="<product_id>".$row['product_id']."</product_id>"; 
$_xml .="<product_name>".$row['product_name']."</product_name>"; 
$_xml .="<price>".$row['price']."</price>"; 
$_xml .= "<category-id>" .$row['category_id']. "</category-id>";
$_xml .= "<description>" .$row['product_description']. "</description>";
$_xml .= "<image> <img src='{data:image/jpeg;base64,".base64_encode($row['product_image'] )."}' height='100' width='200' class='img-thumnail' /> </image>";
$_xml .="</product>"; 
} 
$_xml .="</product_list>"; 
$xmlobj=new SimpleXMLElement($_xml);
ob_clean();
print $xmlobj->asXML();
//include('../includes/footer.php'); ?>
     		