<?php
session_start();
if(!isset($_SESSION['id']))
{
    echo "<script>window.alert('Please login to your account first'); 
    window.location.href= 'members.php';
    </script>";
}
$user_id = (int)$_SESSION['id'];
;
include('../includes/header.php');
include('../includes/config.php');
include('../functions/userFunctions.php');
?>
<?php
//$checkBox = implode(',', $_POST['Image']);
if(isset($_POST['book']))
{
	//$image = addslashes(file_get_contents($_FILES["image"]["tmp_name"]));
	//var_dump($image); die();
	$imgSelection = $_POST['image'];
	$date= $_POST['date'];
	$id= (int)$_SESSION['id'];
	$select= "";
	foreach($imgSelection as $selection)
	{
		$select .= $selection. ",";
	}
	$getProduct= new userFunctions();
	$getProduct->settableName('booked_products');
	$getProduct->setDate($date);
	$getProduct->setproductImage($select);
	$getProduct->setUserId($id);
	$regQuery= $getProduct->bookProducts();
	if($regQuery==true)
	{
		echo "Your order is successfully booked";
	}else{
		echo "error in booking";
	}
}

?>
<form class="registration-form mem-page" method= "post" enctype="multipart/form-data">
	<div id="mem-primary">
	<h2>Products Booking in rent</h2>
	<div class="form-group mem-panel">
		<label class="sr-only">Available products for booking</label>
		<?php $counter= 1; ?>
		<?php
		$viewall= new userFunctions();
		$viewall->  settableName('product_booking');
		$select= $viewall->selectAllItems();
		if($select->num_rows > 0){
			while($row= $select->fetch_assoc()){
				?>
				<div class="mt-box">
				<input type="checkbox" class="form-control" value="image<?php echo $counter; ?>" name= "image[]">
				<?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image'] ).'" height="100" width="200" class="img-thumnail" />';  ?>
				<?php 

				$counter++; ?>
				</div>
				<?php
			}
		}else{
			echo "No record found here";
		}
		?>
	</div>
</div>
<div id="mem-secondary">
<h4>Choose the day you want to book </h4>


	<div class="form-group">
		<label class="sr-only">Date</label>
		<input type="text" class="form-control" name= "date" id= "datepicker" placeholder="Pick Your Date">
	</div>
	<div class="fnt10">
		<button id="submit" class="mt-btn" name= "book" type= "submit">Book Now</button>
	</div>

</div>
</form>
<?php
include('../includes/footer.php');
?>

 <script>
    $(document).ready(function(){
        $('#insert').click(function(){
         var image_name= $('#profile_pic').val();
         if(image_name== ''){
            alert("select image");
        }
        else{
            var extension= $('#image').val().split('.').pop().toLowerCase();
            if(jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1){
                alert('invalid image file');
                $('#image').val('');
                return false;
            }
        }

    });
    });

</script>
