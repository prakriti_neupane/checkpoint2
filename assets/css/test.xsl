<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>Our Store Collection</h2>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th>Product ID</th>
      <th>Product Name</th>
      <th>Price</th>
      <th>Category ID</th>
      <th>Product Description</th>
      <th>Product Image</th>
    </tr>
    <xsl:for-each select="product_list/product">
    <tr>
      <td><xsl:value-of select="product_id"/></td>
      <td><xsl:value-of select="product_name"/></td>
      <td><xsl:value-of select="price"/></td>
      <td><xsl:value-of select="category-id"/></td>
      <td><xsl:value-of select="description"/></td>
      <td><xsl:value-of select="image"/></td>
    </tr>
    </xsl:for-each>
  </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>