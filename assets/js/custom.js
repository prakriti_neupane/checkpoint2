
// menu bar toggle js
$('.toggle-bar i.fa').click(function (){

  $('ul.nav').slideToggle();

});




$( function() {
    $( "#faq" ).accordion();
  } );


$( function() {
    $( "#datepicker" ).datepicker();
  } );

// sticky header
$(window).scroll(function() {
/* Act on the event */
if($(this).scrollTop() > 0){
$('header').addClass('my-sticky');
}else{
$('header').removeClass('my-sticky');
}
});


$('#slider-list').owlCarousel({
nav: false,
dots: true,
items: 1,
autoplay:true,
autoPlaySpeed: 3000,
loop: true,
animateOut: 'fadeOut',
navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
});

$('#business-list, #bank-list, #finance-list, #management-list').owlCarousel({
nav: false,
dots: true,
items: 5,
autoplay:true,
autoPlaySpeed: 3000,
loop: true,
animateOut: 'fadeOut',
navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
});

$('#new-sli').owlCarousel({
nav: false,
dots: true,
items: 5,
autoplay:true,
autoPlaySpeed: 3000,
loop: true,
animateOut: 'fadeOut',
navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
});
// header search js
$(document).ready(function () {
$('li.hdr-search a').click(function (){
$($(this).toggleClass('clicked'));
$("#header-search").slideToggle();
});
});
// table js
$(document).ready(function(){
$('.bt-t').click(function() {
$($(this).toggleClass('clicked'));
$("table").slideToggle();
});
});


var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 3000); // Change image every 2 seconds
}


function myMap() {
  var myCenter = new google.maps.LatLng(27.692234, 85.319309);
  var mapCanvas = document.getElementById("map");
  var mapOptions = {center: myCenter, zoom: 17};
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({position:myCenter});
  marker.setMap(map);

  google.maps.event.addListener(marker,'click',function() {
    var pos = map.getZoom();
    map.setZoom(15);
    map.setCenter(marker.getPosition());
    window.setTimeout(function() {map.setZoom(pos);},3000);
  });
}









// })



 