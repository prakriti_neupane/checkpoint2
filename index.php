<?php
include('includes/config.php');
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>A-Z Clothes</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/owl-carousel.min.css" rel="stylesheet">
     <link href="assets/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <figure class="header-logo">
        <a href="index.php"><img class="logo-header" src="assets/images/logo.jpeg" alt="images"></a>
      </figure>
    </div>
     <div class="toggle-bar">
      <i class="fa fa-bars"></i>
     </div> 
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php">Home</a></li>
      <li><a href="#">About Us</a></li>
      <li><a href="views/contact.php">Contact Us</a></li>
       <li><a href="views/members.php">Sign Up</a></li>
       <li><a href="views/listproduct.php">XML Products Lists</a></li>
       <li><a href="views/angularjs.php">Json/Angular JS</a></li>
       <li><a href="views/faq.php">FAQ</a></li>
       
      <li>
<gcse:search></gcse:search></li>
    </ul>
  </div>
</nav>
  
<div class="container">
<h2 class="w3-center">Welcome To The Store</h2>




<div id="slider-list" class="owl-carousel">
	<div class="slider-one">
  <img src="assets/images/banner1.jpg">
</div>
<div class="slider-one">
  <img src="assets/images/banner5.jpg" style="width:100%">
</div>
<div class="slider-one">
  <img src="assets/images/banner4.jpg" style="width:100%">
</div>
</div>




  <section id="categories">
<div class="container">
  <h2 class="main-hed"> Our Catagories</h2>
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#men" aria-controls="profile" role="tab" data-toggle="tab">Men</a></li>
          <li role="presentation"><a href="#women" aria-controls="messages" role="tab" data-toggle="tab">Women</a></li>
          <li role="presentation"><a href="#kids" aria-controls="settings" role="tab" data-toggle="tab">Kids</a></li>
        </ul>
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="men">
            <div id="bank-list" class="owl-carousel">
              <?php
            include('functions/adminFunctions.php');
            $men= new adminFunctions();
            $men->  settableName('product');
            $men-> setcatId(2);
            $select= $men-> selectbyId();
            //var_dump($select); die();
            if($select->num_rows > 0){
             while($row= $select->fetch_assoc()){
               ?>
              <div class="slider">
                <figure><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['product_image'] ).'" height="200" width="200" class="img-thumnail" />';  ?>
                  <figcaption>
                  <h3><?php echo $row['product_name']; ?></h3>
                  <p><?php echo $row['product_description']; ?><br>
                  </p>
                  </figcaption>
                </figure>
              </div>
              <?php
            }
          }
               ?>
            </div>
          </div>


          <div role="tabpanel" class="tab-pane" id="women">
            <div id="finance-list" class="owl-carousel">
              <?php
            $women= new adminFunctions();
            $women->  settableName('product');
            $women-> setcatId(1);
            $select= $women-> selectbyId();
            //var_dump($select); die();
            if($select->num_rows > 0){
             while($row= $select->fetch_assoc()){
               ?>
              <div class="slider">
                <figure><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['product_image'] ).'" height="200" width="200" class="img-thumnail" />';  ?>
                  <figcaption>
                  <h3><?php echo $row['product_name']; ?></h3>
                  <p><?php echo $row['product_description']; ?><br>
                  </p>
                  </figcaption>
                </figure>
              </div>
              <?php
            }
          }
               ?>

            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="kids">
            <div id="management-list" class="owl-carousel">
              <?php
            $kids= new adminFunctions();
            $kids->  settableName('product');
            $kids-> setcatId(3);
            $select= $kids-> selectbyId();
            //var_dump($select); die();
            if($select->num_rows > 0){
             while($row= $select->fetch_assoc()){
               ?>
              <div class="slider">
                <figure><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['product_image'] ).'" height="200" width="200" class="img-thumnail" />';  ?>
                  <figcaption>
                  <h3><?php echo $row['product_name']; ?></h3>
                  <p><?php echo $row['product_description']; ?><br>
                  </p>
                  </figcaption>
                </figure>
              </div>
              <?php
            }
          }
               ?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
</div>
</div>

<footer>
<div class="container">
<div class="row">
  <div class="foot-logo">
    <div class="col-md-2 col-sm-2">
      <figure>
        <a href="index.php"><img src="assets/images/logo.jpeg" alt="images"></a>
      </figure>
    </div>
  </div>
  <div class="footer-widget">
    <div class="col-md-3 col-sm-3">
      <h4>Recent Activities</h4>
      <ul class="footer-menu">
        <li><a href="../index.php">home</a></li>
        <li><a href="#">Men</a></li>
        <li><a href="#">Women</a></li>
        <li><a href="#">Kids</a></li>
        <li><a href="../views/about.php">About</a></li>
        <li><a href="../views/contact.php">Contact</a></li>
      </ul>
    </div>
  </div>
  <div class="footer-widget">
    <div class="col-md-3 col-sm-3">
      <div class="contact">
        <a href="contact-us.html"><i class="fa fa-envelope-o"></i><span>Contact Us Now ❯</span></a>
      </div>
      <div class="social-sec">
        <ul>
          <li> <a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-youtube"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>

        </ul>
      </div>
    </div>
  </div>
</div>
<div class="col-md-8 col-md-offset-2">
  <div class="textwidget">© Copyright A-Z Clothing Store.All Rights Reserved.Design
</div>
</div>
</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/owl-carousel.min.js"></script>
<script src="assets/js/custom.js"></script>

<script>
  
  (function() {
    var cx = '005743141313764961449:ny1joo668ak';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
</body>
</html>
